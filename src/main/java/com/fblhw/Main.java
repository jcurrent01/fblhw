package com.fblhw;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Main {
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                printHelp();
                return;
            }
            if (args.length > 1 && args[1].startsWith("-")) {
                printHelp();
                return;
            }
            //maybe return a connection here?
            DatabaseUtil.checkDb();
            switch (args[0]) {
                case "-i":
                case "--init": {
                    initializeFile(args[1]);
                }
                case "-d":
                case "--delta": {
                    deltaFile(args[1]);
                }
                case "-cr":
                case "--clear-and-reset": {
                    DatabaseUtil.clearAndResetDb();
                }
                default:
                    printHelp();
            }
        } catch (Exception ex) {
            System.out.println("Something went horribly wrong, here's why:");
            ex.printStackTrace();
            System.exit(1);
        }
    }

    private static void printHelp() {
        System.out.println("usage: fblhw flag [url]");
        System.out.println();
        System.out.println("flags:");
        System.out.println("requires url:");
        System.out.println("\t-i, --init\t\tinitializes a new roster file");
        System.out.println("\t-d, --delta\t\truns a delta file on an existing roster");
        System.out.println("does not require url:");
        System.out.println("\t-cr, --clear-and-reset\tclears and resets the app db");
    }

    //TODO - extract similar logic into different method after methods are more complete
    private static void initializeFile(String urlString) throws IOException {
        URL url = new URL(urlString);
        final Reader reader = new InputStreamReader(new BOMInputStream(url.openStream()), "UTF-8");
        CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        Stream<CSVRecord> stream = StreamSupport.stream(parser.spliterator(), false);
        stream.forEach((CSVRecord record) -> {
            //Process student data
            //Process teacher data
            //Process enrollment data
        });
    }

    private static void deltaFile(String urlString) throws IOException {
        URL url = new URL(urlString);
        final Reader reader = new InputStreamReader(new BOMInputStream(url.openStream()), "UTF-8");
        CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader());
        Stream<CSVRecord> stream = StreamSupport.stream(parser.spliterator(), false);
        stream.forEach((CSVRecord record) -> {
            //Process adds and deletes
            //Process student data
            //Process teacher data
            //Process enrollment data
        });
    }
}
