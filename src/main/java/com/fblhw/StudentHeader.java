package com.fblhw;

// TODO - Create TeacherHeader and EnrollmentHeader enums
public enum StudentHeader {
    // TODO - fill in with the rest of the columns
    STUDENT_ID("StudentID");

    private String name;

    StudentHeader(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
